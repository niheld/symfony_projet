<?php

namespace App\Repository;

use App\Entity\SaisonHotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SaisonHotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method SaisonHotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method SaisonHotel[]    findAll()
 * @method SaisonHotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaisonHotelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SaisonHotel::class);
    }

    // /**
    //  * @return SaisonHotel[] Returns an array of SaisonHotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SaisonHotel
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
