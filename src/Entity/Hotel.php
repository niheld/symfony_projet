<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HotelRepository")
 * @ApiResource
 */
class Hotel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ville;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaisonHotel", mappedBy="id_hotel")
     */
    private $saisonHotels;

    public function __construct()
    {
        $this->saisonHotels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getVille(): ?Ville
    {
        return $this->ville;
    }

    public function setVille(?Ville $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return Collection|SaisonHotel[]
     */
    public function getSaisonHotels(): Collection
    {
        return $this->saisonHotels;
    }

    public function addSaisonHotel(SaisonHotel $saisonHotel): self
    {
        if (!$this->saisonHotels->contains($saisonHotel)) {
            $this->saisonHotels[] = $saisonHotel;
            $saisonHotel->setIdHotel($this);
        }

        return $this;
    }

    public function removeSaisonHotel(SaisonHotel $saisonHotel): self
    {
        if ($this->saisonHotels->contains($saisonHotel)) {
            $this->saisonHotels->removeElement($saisonHotel);
            // set the owning side to null (unless already changed)
            if ($saisonHotel->getIdHotel() === $this) {
                $saisonHotel->setIdHotel(null);
            }
        }

        return $this;
    }

    public function __toString(){

        return $this->nom;
       
    }
}
