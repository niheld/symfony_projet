<?php

namespace App\Controller;

use App\Entity\SaisonHotel;
use App\Form\SaisonHotelType;
use App\Repository\SaisonHotelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/saison/hotel")
 */
class SaisonHotelController extends AbstractController
{
    /**
     * @Route("/", name="saison_hotel_index", methods={"GET"})
     */
    public function index(SaisonHotelRepository $saisonHotelRepository): Response
    {
        return $this->render('saison_hotel/index.html.twig', [
            'saison_hotels' => $saisonHotelRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="saison_hotel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $saisonHotel = new SaisonHotel();
        $form = $this->createForm(SaisonHotelType::class, $saisonHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($saisonHotel);
            $entityManager->flush();

            return $this->redirectToRoute('saison_hotel_index');
        }

        return $this->render('saison_hotel/new.html.twig', [
            'saison_hotel' => $saisonHotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="saison_hotel_show", methods={"GET"})
     */
    public function show(SaisonHotel $saisonHotel): Response
    {
        return $this->render('saison_hotel/show.html.twig', [
            'saison_hotel' => $saisonHotel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="saison_hotel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SaisonHotel $saisonHotel): Response
    {
        $form = $this->createForm(SaisonHotelType::class, $saisonHotel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('saison_hotel_index');
        }

        return $this->render('saison_hotel/edit.html.twig', [
            'saison_hotel' => $saisonHotel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="saison_hotel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SaisonHotel $saisonHotel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$saisonHotel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($saisonHotel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('saison_hotel_index');
    }
}
